JOYASUS Modifications
=====================

Describes my modifications to the fan-cooled protection goggles (used by people playing airsoft)

Items needed
------------

 * [Goggles](https://www.amazon.de/gp/product/B076MTW6HF/)
 * [Microcontroller](https://www.amazon.de/gp/product/B079TZZMR4/)
 * [Sensor](https://www.amazon.de/gp/product/B07V9PJP1K/)

Modifications
-------------

 * re-install fan to blow outwards
 * replace battery compartment with microcontroller, powered via USB powerbank
 * add humidity/temperature sensor to the microcontroller
